# Capteur-co2

Import from Gregoire Rinolfi's CO2 sensor code : https://co2.rinolfi.ch/
To be compared and improved with other forks, such as : https://github.com/nOfOKUz/capteurCO2 or https://github.com/fvanderbiest/ttgo-s8-rinolfi-sketch

## Fork Motivation
Change data uforwarding from Thingspeak to **PushData.io** API.

## Usage
1. Create your pushdata.io account with your email address
2. Choose a topic and set the url in the code
3. Change Wifi settings according to yours
4. You're good to go ! Upload sketch on the TTGO board, fitted with the SenseAir S8 CO2 Sensor.


## Authors and acknowledgment
All credit goes to Gregoire Rinolfi for publishing the code along with the hardware setup.

## License
As Gregoire did not specify a licence when publishing his code, I do not have any rights to put a new one on this modified version. Use at your own (probably very limited) legal risks.
